﻿namespace Steganography_APP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OriginalFile_GB = new System.Windows.Forms.GroupBox();
            this.OriginalFileLoad_btn = new System.Windows.Forms.Button();
            this.OriginalFileSize_lbl = new System.Windows.Forms.Label();
            this.OriginalFileName_lbl = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.EncodedFile_GB = new System.Windows.Forms.GroupBox();
            this.EncodedFileLoad_btn = new System.Windows.Forms.Button();
            this.EncodedFileSize_lbl = new System.Windows.Forms.Label();
            this.EncodedFileName_lbl = new System.Windows.Forms.Label();
            this.EncodingSettings_GB = new System.Windows.Forms.GroupBox();
            this.NewSIze_lbl = new System.Windows.Forms.Label();
            this.ColorInfo_lbl = new System.Windows.Forms.Label();
            this.setBlue_nud = new System.Windows.Forms.NumericUpDown();
            this.setGreen_nud = new System.Windows.Forms.NumericUpDown();
            this.setRed_nud = new System.Windows.Forms.NumericUpDown();
            this.Decode_btn = new System.Windows.Forms.Button();
            this.Encode_btn = new System.Windows.Forms.Button();
            this.Save_btn = new System.Windows.Forms.Button();
            this.Info_btn = new System.Windows.Forms.Button();
            this.OriginaPicture_pb = new System.Windows.Forms.PictureBox();
            this.NewPicture_pb = new System.Windows.Forms.PictureBox();
            this.OriginalPicture_lbl = new System.Windows.Forms.Label();
            this.NewPicture_lbl = new System.Windows.Forms.Label();
            this.OriginalFile_GB.SuspendLayout();
            this.EncodedFile_GB.SuspendLayout();
            this.EncodingSettings_GB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setBlue_nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setGreen_nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.setRed_nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginaPicture_pb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewPicture_pb)).BeginInit();
            this.SuspendLayout();
            // 
            // OriginalFile_GB
            // 
            this.OriginalFile_GB.Controls.Add(this.OriginalFileLoad_btn);
            this.OriginalFile_GB.Controls.Add(this.OriginalFileSize_lbl);
            this.OriginalFile_GB.Controls.Add(this.OriginalFileName_lbl);
            this.OriginalFile_GB.Location = new System.Drawing.Point(735, 12);
            this.OriginalFile_GB.Name = "OriginalFile_GB";
            this.OriginalFile_GB.Size = new System.Drawing.Size(250, 86);
            this.OriginalFile_GB.TabIndex = 0;
            this.OriginalFile_GB.TabStop = false;
            this.OriginalFile_GB.Text = "Original File";
            // 
            // OriginalFileLoad_btn
            // 
            this.OriginalFileLoad_btn.Location = new System.Drawing.Point(6, 34);
            this.OriginalFileLoad_btn.Name = "OriginalFileLoad_btn";
            this.OriginalFileLoad_btn.Size = new System.Drawing.Size(60, 40);
            this.OriginalFileLoad_btn.TabIndex = 7;
            this.OriginalFileLoad_btn.Text = "Load";
            this.OriginalFileLoad_btn.UseVisualStyleBackColor = true;
            this.OriginalFileLoad_btn.Click += new System.EventHandler(this.OriginalFIleLoad_btn_Click);
            // 
            // OriginalFileSize_lbl
            // 
            this.OriginalFileSize_lbl.AutoSize = true;
            this.OriginalFileSize_lbl.Location = new System.Drawing.Point(72, 57);
            this.OriginalFileSize_lbl.Name = "OriginalFileSize_lbl";
            this.OriginalFileSize_lbl.Size = new System.Drawing.Size(43, 17);
            this.OriginalFileSize_lbl.TabIndex = 8;
            this.OriginalFileSize_lbl.Text = "Size: ";
            // 
            // OriginalFileName_lbl
            // 
            this.OriginalFileName_lbl.AutoSize = true;
            this.OriginalFileName_lbl.Location = new System.Drawing.Point(72, 34);
            this.OriginalFileName_lbl.Name = "OriginalFileName_lbl";
            this.OriginalFileName_lbl.Size = new System.Drawing.Size(53, 17);
            this.OriginalFileName_lbl.TabIndex = 7;
            this.OriginalFileName_lbl.Text = "Name: ";
            // 
            // EncodedFile_GB
            // 
            this.EncodedFile_GB.Controls.Add(this.EncodedFileLoad_btn);
            this.EncodedFile_GB.Controls.Add(this.EncodedFileSize_lbl);
            this.EncodedFile_GB.Controls.Add(this.EncodedFileName_lbl);
            this.EncodedFile_GB.Location = new System.Drawing.Point(1001, 12);
            this.EncodedFile_GB.Name = "EncodedFile_GB";
            this.EncodedFile_GB.Size = new System.Drawing.Size(250, 86);
            this.EncodedFile_GB.TabIndex = 1;
            this.EncodedFile_GB.TabStop = false;
            this.EncodedFile_GB.Text = "Encoded File";
            // 
            // EncodedFileLoad_btn
            // 
            this.EncodedFileLoad_btn.Location = new System.Drawing.Point(6, 32);
            this.EncodedFileLoad_btn.Name = "EncodedFileLoad_btn";
            this.EncodedFileLoad_btn.Size = new System.Drawing.Size(60, 42);
            this.EncodedFileLoad_btn.TabIndex = 9;
            this.EncodedFileLoad_btn.Text = "Load";
            this.EncodedFileLoad_btn.UseVisualStyleBackColor = true;
            this.EncodedFileLoad_btn.Click += new System.EventHandler(this.EncodedFIleLoad_btn_Click);
            // 
            // EncodedFileSize_lbl
            // 
            this.EncodedFileSize_lbl.AutoSize = true;
            this.EncodedFileSize_lbl.Location = new System.Drawing.Point(72, 57);
            this.EncodedFileSize_lbl.Name = "EncodedFileSize_lbl";
            this.EncodedFileSize_lbl.Size = new System.Drawing.Size(43, 17);
            this.EncodedFileSize_lbl.TabIndex = 9;
            this.EncodedFileSize_lbl.Text = "Size: ";
            // 
            // EncodedFileName_lbl
            // 
            this.EncodedFileName_lbl.AutoSize = true;
            this.EncodedFileName_lbl.Location = new System.Drawing.Point(72, 34);
            this.EncodedFileName_lbl.Name = "EncodedFileName_lbl";
            this.EncodedFileName_lbl.Size = new System.Drawing.Size(53, 17);
            this.EncodedFileName_lbl.TabIndex = 8;
            this.EncodedFileName_lbl.Text = "Name: ";
            // 
            // EncodingSettings_GB
            // 
            this.EncodingSettings_GB.Controls.Add(this.NewSIze_lbl);
            this.EncodingSettings_GB.Controls.Add(this.ColorInfo_lbl);
            this.EncodingSettings_GB.Controls.Add(this.setBlue_nud);
            this.EncodingSettings_GB.Controls.Add(this.setGreen_nud);
            this.EncodingSettings_GB.Controls.Add(this.setRed_nud);
            this.EncodingSettings_GB.Location = new System.Drawing.Point(735, 104);
            this.EncodingSettings_GB.Name = "EncodingSettings_GB";
            this.EncodingSettings_GB.Size = new System.Drawing.Size(516, 184);
            this.EncodingSettings_GB.TabIndex = 2;
            this.EncodingSettings_GB.TabStop = false;
            this.EncodingSettings_GB.Text = "Encoding Settings";
            // 
            // NewSIze_lbl
            // 
            this.NewSIze_lbl.AutoSize = true;
            this.NewSIze_lbl.Location = new System.Drawing.Point(184, 12);
            this.NewSIze_lbl.Name = "NewSIze_lbl";
            this.NewSIze_lbl.Size = new System.Drawing.Size(74, 17);
            this.NewSIze_lbl.TabIndex = 8;
            this.NewSIze_lbl.Text = "New Size: ";
            // 
            // ColorInfo_lbl
            // 
            this.ColorInfo_lbl.AutoSize = true;
            this.ColorInfo_lbl.Location = new System.Drawing.Point(6, 54);
            this.ColorInfo_lbl.Name = "ColorInfo_lbl";
            this.ColorInfo_lbl.Size = new System.Drawing.Size(138, 17);
            this.ColorInfo_lbl.TabIndex = 7;
            this.ColorInfo_lbl.Text = "Red     Green    Blue";
            // 
            // setBlue_nud
            // 
            this.setBlue_nud.Location = new System.Drawing.Point(106, 29);
            this.setBlue_nud.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.setBlue_nud.Name = "setBlue_nud";
            this.setBlue_nud.Size = new System.Drawing.Size(44, 22);
            this.setBlue_nud.TabIndex = 2;
            // 
            // setGreen_nud
            // 
            this.setGreen_nud.Location = new System.Drawing.Point(56, 29);
            this.setGreen_nud.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.setGreen_nud.Name = "setGreen_nud";
            this.setGreen_nud.Size = new System.Drawing.Size(44, 22);
            this.setGreen_nud.TabIndex = 1;
            // 
            // setRed_nud
            // 
            this.setRed_nud.Location = new System.Drawing.Point(6, 29);
            this.setRed_nud.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.setRed_nud.Name = "setRed_nud";
            this.setRed_nud.Size = new System.Drawing.Size(44, 22);
            this.setRed_nud.TabIndex = 0;
            // 
            // Decode_btn
            // 
            this.Decode_btn.Location = new System.Drawing.Point(870, 294);
            this.Decode_btn.Name = "Decode_btn";
            this.Decode_btn.Size = new System.Drawing.Size(135, 40);
            this.Decode_btn.TabIndex = 3;
            this.Decode_btn.Text = "Decode";
            this.Decode_btn.UseVisualStyleBackColor = true;
            // 
            // Encode_btn
            // 
            this.Encode_btn.Location = new System.Drawing.Point(735, 294);
            this.Encode_btn.Name = "Encode_btn";
            this.Encode_btn.Size = new System.Drawing.Size(129, 40);
            this.Encode_btn.TabIndex = 4;
            this.Encode_btn.Text = "Encode";
            this.Encode_btn.UseVisualStyleBackColor = true;
            this.Encode_btn.Click += new System.EventHandler(this.Encode_btn_Click);
            // 
            // Save_btn
            // 
            this.Save_btn.Location = new System.Drawing.Point(1011, 294);
            this.Save_btn.Name = "Save_btn";
            this.Save_btn.Size = new System.Drawing.Size(115, 40);
            this.Save_btn.TabIndex = 5;
            this.Save_btn.Text = "Save";
            this.Save_btn.UseVisualStyleBackColor = true;
            // 
            // Info_btn
            // 
            this.Info_btn.Location = new System.Drawing.Point(1135, 294);
            this.Info_btn.Name = "Info_btn";
            this.Info_btn.Size = new System.Drawing.Size(116, 40);
            this.Info_btn.TabIndex = 6;
            this.Info_btn.Text = "Info";
            this.Info_btn.UseVisualStyleBackColor = true;
            // 
            // OriginaPicture_pb
            // 
            this.OriginaPicture_pb.Location = new System.Drawing.Point(12, 12);
            this.OriginaPicture_pb.Name = "OriginaPicture_pb";
            this.OriginaPicture_pb.Size = new System.Drawing.Size(320, 240);
            this.OriginaPicture_pb.TabIndex = 7;
            this.OriginaPicture_pb.TabStop = false;
            // 
            // NewPicture_pb
            // 
            this.NewPicture_pb.Location = new System.Drawing.Point(371, 12);
            this.NewPicture_pb.Name = "NewPicture_pb";
            this.NewPicture_pb.Size = new System.Drawing.Size(320, 240);
            this.NewPicture_pb.TabIndex = 8;
            this.NewPicture_pb.TabStop = false;
            // 
            // OriginalPicture_lbl
            // 
            this.OriginalPicture_lbl.AutoSize = true;
            this.OriginalPicture_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OriginalPicture_lbl.Location = new System.Drawing.Point(78, 272);
            this.OriginalPicture_lbl.Name = "OriginalPicture_lbl";
            this.OriginalPicture_lbl.Size = new System.Drawing.Size(195, 29);
            this.OriginalPicture_lbl.TabIndex = 9;
            this.OriginalPicture_lbl.Text = "Original Picture";
            // 
            // NewPicture_lbl
            // 
            this.NewPicture_lbl.AutoSize = true;
            this.NewPicture_lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NewPicture_lbl.Location = new System.Drawing.Point(459, 272);
            this.NewPicture_lbl.Name = "NewPicture_lbl";
            this.NewPicture_lbl.Size = new System.Drawing.Size(155, 29);
            this.NewPicture_lbl.TabIndex = 10;
            this.NewPicture_lbl.Text = "New Picture";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1267, 346);
            this.Controls.Add(this.NewPicture_lbl);
            this.Controls.Add(this.OriginalPicture_lbl);
            this.Controls.Add(this.NewPicture_pb);
            this.Controls.Add(this.OriginaPicture_pb);
            this.Controls.Add(this.Info_btn);
            this.Controls.Add(this.Save_btn);
            this.Controls.Add(this.Encode_btn);
            this.Controls.Add(this.Decode_btn);
            this.Controls.Add(this.EncodingSettings_GB);
            this.Controls.Add(this.EncodedFile_GB);
            this.Controls.Add(this.OriginalFile_GB);
            this.Name = "MainForm";
            this.Text = "Encode";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.OriginalFile_GB.ResumeLayout(false);
            this.OriginalFile_GB.PerformLayout();
            this.EncodedFile_GB.ResumeLayout(false);
            this.EncodedFile_GB.PerformLayout();
            this.EncodingSettings_GB.ResumeLayout(false);
            this.EncodingSettings_GB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.setBlue_nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setGreen_nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.setRed_nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OriginaPicture_pb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NewPicture_pb)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox OriginalFile_GB;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox EncodedFile_GB;
        private System.Windows.Forms.GroupBox EncodingSettings_GB;
        private System.Windows.Forms.Button OriginalFileLoad_btn;
        private System.Windows.Forms.Label OriginalFileSize_lbl;
        private System.Windows.Forms.Label OriginalFileName_lbl;
        private System.Windows.Forms.Button EncodedFileLoad_btn;
        private System.Windows.Forms.Label EncodedFileSize_lbl;
        private System.Windows.Forms.Label EncodedFileName_lbl;
        private System.Windows.Forms.NumericUpDown setBlue_nud;
        private System.Windows.Forms.NumericUpDown setGreen_nud;
        private System.Windows.Forms.NumericUpDown setRed_nud;
        private System.Windows.Forms.Button Decode_btn;
        private System.Windows.Forms.Button Encode_btn;
        private System.Windows.Forms.Button Save_btn;
        private System.Windows.Forms.Button Info_btn;
        private System.Windows.Forms.Label ColorInfo_lbl;
        private System.Windows.Forms.Label NewSIze_lbl;
        private System.Windows.Forms.PictureBox OriginaPicture_pb;
        private System.Windows.Forms.PictureBox NewPicture_pb;
        private System.Windows.Forms.Label OriginalPicture_lbl;
        private System.Windows.Forms.Label NewPicture_lbl;
    }
}

