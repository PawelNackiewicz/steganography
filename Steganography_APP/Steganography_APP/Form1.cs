﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Steganography_APP
{
    public partial class MainForm : Form
    {
        #region global variables

            private bool isLoadedPicture = false;
            private bool isLoadedFile = false;

            private string nameOfTheOriginalFile;
            private string pathOfTheOriginalFile;
            private string nameOfTheEncodedlFile;
            private string pathOfTheEncodedFile;

        #endregion global variables
        private bool checkStatusFiles(bool PictureStatus,bool FileStatus)
        {
            if(!PictureStatus)
            {
                MessageBox.Show("Select a picture! ","Error!",MessageBoxButtons.OK, MessageBoxIcon.Information);
             
            }
            if(!FileStatus)
            {
                MessageBox.Show("Select a file to encode! ", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return (PictureStatus & FileStatus);
        }
        
        public MainForm()
        {
            InitializeComponent();
        }


        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void OriginalFIleLoad_btn_Click(object sender, EventArgs e)
        {
           
            OpenFileDialog OpenFile = new OpenFileDialog();
            OpenFile.Filter = "Bitmap file (*bmp)|*.bmp";
            if (DialogResult.OK == OpenFile.ShowDialog())
            {
                OriginaPicture_pb.Image = new Bitmap(OpenFile.FileName);
                setInfoAboutOriginalPicture(OpenFile);
                isLoadedPicture = true;
               
            }
          

        }
        private void setInfoAboutOriginalPicture(OpenFileDialog file)
        {
            FileInfo myFile = new FileInfo(file.FileName);
            OriginalFileName_lbl.Text = "Name: " + myFile.Name;
            OriginalFileSize_lbl.Text = "Size: " + myFile.Length.ToString() + " KB";
            //set nameOfTheOriginalFile
            nameOfTheOriginalFile = myFile.Name;
            pathOfTheOriginalFile = file.FileName;
        }
     

        private void EncodedFIleLoad_btn_Click(object sender, EventArgs e)
        {
            OpenFileDialog OpenFile = new OpenFileDialog();
            OpenFile.Filter = "All files (*.*)|*.*";

            if (DialogResult.OK == OpenFile.ShowDialog())
            {
                setInfoAboutEncodedFile(OpenFile);
                isLoadedFile = true;
            }
        }
        private void setInfoAboutEncodedFile(OpenFileDialog file)
        {
            FileInfo myFile = new FileInfo(file.FileName);
            EncodedFileName_lbl.Text = "Name: " + myFile.Name;
            EncodedFileSize_lbl.Text = "Size: " + myFile.Length.ToString() + " KB";
            nameOfTheEncodedlFile = myFile.Name;
            pathOfTheEncodedFile = file.FileName;
        }
        private ushort setColorValue(NumericUpDown nud)
        {
            ushort value =  (ushort)Convert.ToInt16(nud.Value);
            return value;       
        }
        private FileStream createFileStream(string name)
        {
            FileInfo fileInfo = new FileInfo(name);
            FileStream fileStream = fileInfo.OpenRead();
            return fileStream;
        }
        public int BitArrayToInt(BitArray bits, int startIndex)
        {
            int b = 0;
            for (int i = startIndex, mn = 1; i < startIndex + 8; i++, mn = mn * 2)
                if (bits.Get(i)) b += mn;

            return b;
        }

       
        

        public byte[] createAndCopyArrays(FileStream fileToCalculateLenght,ushort redByteValue, ushort greenByteValue, ushort blueByteValue)
        {
            int lenghtOfFile = (int)fileToCalculateLenght.Length;
            byte[] arrayOfFileBytes = new byte[lenghtOfFile];
            byte[] lenghtOfFileInBytes = System.BitConverter.GetBytes(lenghtOfFile); // 4 bajty
            byte[] redBytes = System.BitConverter.GetBytes(redByteValue);                 // 1 bajt
            byte[] greenBytes = System.BitConverter.GetBytes(greenByteValue);                   // 1 bajt
            byte[] blueBytes = System.BitConverter.GetBytes(blueByteValue);                  // 1 bajt
            byte[] encodingBytes = new byte[lenghtOfFile + 7];

            Array.Copy(lenghtOfFileInBytes, 0, encodingBytes, 0, 4);
            Array.Copy(redBytes, 0, encodingBytes, 4, 1);
            Array.Copy(greenBytes, 0, encodingBytes, 5, 1);
            Array.Copy(blueBytes, 0, encodingBytes, 6, 1);
            Array.Copy(arrayOfFileBytes, 0, encodingBytes, 7, lenghtOfFile);

            return encodingBytes;
        }
     
        public Bitmap encoding(ushort redByteValue,ushort greenByteValue,ushort blueByteValue, byte[]encodingBytes)
        {
            Bitmap newPicture = new Bitmap(pathOfTheOriginalFile);
            Color colorValue;
            BitArray encodingBytesArray = new BitArray(encodingBytes);

            int numberOfBit = 0;
            for (int i = 0; i < newPicture.Width && numberOfBit < encodingBytesArray.Length; i++)
            {
                for (int j = 0; j < newPicture.Height && numberOfBit < encodingBytesArray.Length; j++)
                {

                    colorValue = newPicture.GetPixel(i, j);
                    BitArray red = new BitArray(System.BitConverter.GetBytes(colorValue.R));
                    BitArray green = new BitArray(System.BitConverter.GetBytes(colorValue.G));
                    BitArray blue = new BitArray(System.BitConverter.GetBytes(colorValue.B));

                    for (int w = 0; w < redByteValue && numberOfBit < encodingBytesArray.Length; w++, numberOfBit++)
                        red[w] = encodingBytesArray[numberOfBit];

                    for (int w = 0; w < greenByteValue && numberOfBit < encodingBytesArray.Length; w++, numberOfBit++)
                        green[w] = encodingBytesArray[numberOfBit];

                    for (int w = 0; w < blueByteValue && numberOfBit < encodingBytesArray.Length; w++, numberOfBit++)
                        blue[w] = encodingBytesArray[numberOfBit];

                    int intRED = BitArrayToInt(red, 0);
                    int intGREEN = BitArrayToInt(green, 0);
                    int intBLUE = BitArrayToInt(blue, 0);

                    newPicture.SetPixel(i, j, Color.FromArgb(intRED, intGREEN, intBLUE));
                }
            }
            return newPicture;
        }
      


        private void Encode_btn_Click(object sender, EventArgs e)
        {
      
            checkStatusFiles(isLoadedPicture,isLoadedFile);

            //Console.WriteLine("R: " + redByteValue + " G: " + greenByteValue + " B: " + blueByteValue+nameOfTheOriginalFile);

            try
            {

                ushort redByteValue = setColorValue(setRed_nud);
                ushort greenByteValue = setColorValue(setGreen_nud);
                ushort blueByteValue = setColorValue(setBlue_nud);

                FileStream myFile = createFileStream(pathOfTheOriginalFile);
                byte[] encodingBytes = createAndCopyArrays(myFile,redByteValue,greenByteValue,blueByteValue);
                myFile.Close();
                Console.WriteLine("Done!");
                NewPicture_pb.Image = encoding(redByteValue, greenByteValue, blueByteValue, encodingBytes); ;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }

}
